﻿using System;
using NUnit.Framework;
using PassphraseGenerator;

namespace PassphraseGenerator.Tests
{
    [TestFixture]
    public class RollerTests
    {
        [Test]
        public void should_get_roll()
        {
            var roller = new Roller();
            var roll = roller.GetRoll();

            Assert.IsNotEmpty(roll);
        }

        [Test]
        public void should_be_different()
        {
            var roller = new Roller();
            var roll1 = roller.GetRoll();
            var roll2 = roller.GetRoll();

            Assert.AreNotEqual(roll1, roll2);
        }

        [Test]
        public void should_get_passphrase()
        {
            var roller = new Roller();
            var word = roller.GetPassphrase();

            Assert.IsNotEmpty(word);
            
        }
    }
}
