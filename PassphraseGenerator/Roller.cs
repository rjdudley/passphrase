﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PassphraseGenerator
{
    public class Roller
    {
        // http://msdn.microsoft.com/en-us/library/system.security.cryptography.rngcryptoserviceprovider(v=vs.110).aspx

        public string GetPassphrase()
        {
            var list = new Wordlist();

            string firstRoll = GetRoll();
            string secondRoll = GetRoll();

            var word1 = list.Words.First(w => w.Key == firstRoll);
            var word2 = list.Words.First(w => w.Key == secondRoll);
            
            return string.Format("{0} {1} {2}", word1.Value, word2.Value, secondRoll.Substring(3,2));
        }

        internal string GetRoll()
        {
            int r1 = Roll();
            int r2 = Roll();
            int r3 = Roll();
            int r4 = Roll();
            int r5 = Roll();

            string roll = string.Format("{0}{1}{2}{3}{4}", r1, r2, r3, r4, r5);

            return roll;
        }


        private int Roll()
        {
            byte[] randomNumber = new byte[1];

            int side = 0;
            RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
            rngCsp.GetBytes(randomNumber);

            int rn = (int)randomNumber[0];

            while (side == 0)
            {
                if (rn >= 1 && rn <= 42)
                {
                    side = 1;
                }

                if (rn >= 43 && rn <= 84)
                {
                    side = 2;
                }

                if (rn >= 85 && rn <= 126)
                {
                    side = 3;
                }

                if (rn >= 127 && rn <= 168)
                {
                    side = 4;
                }

                if (rn >= 169 && rn <= 210)
                {
                    side = 5;
                }

                if (rn >= 211 && rn <= 252)
                {
                    side = 6;
                }
            }

            return side;
        }

    }
}
