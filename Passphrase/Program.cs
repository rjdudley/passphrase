﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PassphraseGenerator;

namespace Passphrase
{
    class Program
    {
        static void Main(string[] args)
        {
            var roller = new Roller();

            Console.WriteLine(roller.GetPassphrase());
            Console.ReadLine();
        }
    }
}
